### Classe #3 = "From wet to dry-lab"

#### Biologia Forense 2022-2023

![Logo FCUL](c03_assets/logo-FCUL.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->Sanger sequencing reminder
* &shy;<!-- .element: class="fragment" -->Chromatogram interpretation
* &shy;<!-- .element: class="fragment" -->From chromatograms to text files
* &shy;<!-- .element: class="fragment" -->The FASTA format
* &shy;<!-- .element: class="fragment" -->Biological sequences databases
    * &shy;<!-- .element: class="fragment" -->Sequence identification
    * &shy;<!-- .element: class="fragment" -->Sequence searching
* &shy;<!-- .element: class="fragment" -->Practical examples

---

### The wet-lab is done. Now what?

![DNA lab](c03_assets/DNA_lab.jpg)

---

### Sanger sequencing

&shy;<!-- .element: class="fragment" -->![Sanger sequencing](c03_assets/sanger-sequencing.png)

|||

### Sanger sequencing

* &shy;<!-- .element: class="fragment" -->Golden Standard
* &shy;<!-- .element: class="fragment" -->High quality
* &shy;<!-- .element: class="fragment" -->Low throughput
* &shy;<!-- .element: class="fragment" -->Large files
    * &shy;<!-- .element: class="fragment" -->How do we read interpret them?
      * &shy;<!-- .element: class="fragment" -->[CutePeaks](https://github.com/labsquare/CutePeaks)
      * &shy;<!-- .element: class="fragment" -->[SeqTrace](https://github.com/stuckyb/seqtrace)
      * &shy;<!-- .element: class="fragment" -->[Sequencher](http://genecodes.com/sequencher)
      * &shy;<!-- .element: class="fragment" -->[Codon Code Aligner](https://www.codoncode.com/aligner/)

&shy;<!-- .element: class="fragment" -->![Chromatogram](c03_assets/chroma.png)

---

### Hands on!

* &shy;<!-- .element: class="fragment" -->Today we will use [SeqTrace](https://github.com/stuckyb/seqtrace)
&shy;<!-- .element: class="fragment" -->![Chromatogram](c03_assets/seqtrace.png)
* &shy;<!-- .element: class="fragment" -->To interpret [Trace files](c03_assets/Traces_2023.zip)

---

### If only things were straightforward...

![IUPAC](c03_assets/IUPAC.png)

IUPAC codes

|||

## Nucleotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>-</td>
<td>gap</td>
</tr>
</table>

---


### The FASTA format

* &shy;<!-- .element: class="fragment" -->The [FASTA](https://zhanglab.ccmb.med.umich.edu/FASTA/) format is arguably the most used sequence format in Bioinformatics
* &shy;<!-- .element: class="fragment" -->Simple
* &shy;<!-- .element: class="fragment" -->Small
* &shy;<!-- .element: class="fragment" -->Readable
    * &shy;<!-- .element: class="fragment" -->Humans
    * &shy;<!-- .element: class="fragment" -->Machines

|||

### What the FASTA format looks like

<div class="fragment">

``` plaintext
>First Sequence name
SEQUENCE
>Second Sequence name
SEQUENCE
>Nth Sequence name
SEQUENCE
```

</div>
<div class="fragment">

``` text
>AB080259.1 Calliphora dubia mitochondrial COI gene for cytochrome oxidase I, partial cds, isolate:CDWA5
AGCTAACTCTTCCGTAGATATTATCCTTCATGATACTTATTATGTAGTTGCTCATTTCCATTATGTTTTA
TCAATAGGAGCTGTATTTGCCATTATAGCAGGATTTGTTCATTGATACCCTCTATTTACAGGTTTAACTT
TAAATGGAAAAATACTAAAAAGTCAATTTACTATTATATTTATTGGAGTAAATATCACATTTTTCCCTCA
ACACTTTTTAGGATTAGCAGGAATACCTCGACGATATTCAGATTATCCAGATGCTTACACAGCTTGAA
>AB080258.1 Calliphora dubia mitochondrial COI gene for cytochrome oxidase I, partial cds, isolate:CDWA2
AGCTAACTCTTCCGTAGATATTATCCTTCATGATACTTATTATGTAGTTGCTCATTTCCATTATGTTTTA
TCAATAGGAGCTGTATTTGCCATTATAGCAGGATTTGTTCATTGATACCCTCTATTTACAGGTTTAACTT
TAAATGGAAAAATACTAAAAAGTCAATTTACTATTATATTTATTGGAGTAAATATTACATTTTTCCCTCA
ACACTTTTTAGGATTAGCAGGAATACCTCGACGATATTCAGATTATCCAGATGCTTACACAGCTTGGA
```

</div>

&shy;<!-- .element: class="fragment" -->Grab your FASTA file [here](c03_assets/fly.fasta)

---

### Viewing the sequence with a GUI

[![AliView](c03_assets/aliview.png)](https://ormbunkar.se/aliview/)

&shy;<!-- .element: class="fragment" -->Try opening a FASTA file with a text editor, and with Aliview

---

### DNA sequence databases

* &shy;<!-- .element: class="fragment" -->[NCBI (USA)](https://www.ncbi.nlm.nih.gov/)
* &shy;<!-- .element: class="fragment" -->[EMBL (Europe)](https://www.embl.org/)
* &shy;<!-- .element: class="fragment" -->[DDBJ (Japan)](https://www.ddbj.nig.ac.jp)
  * &shy;<!-- .element: class="fragment" -->Data repositories
  * &shy;<!-- .element: class="fragment" -->Replicated
  * &shy;<!-- .element: class="fragment" -->Queryable

&shy;<!-- .element: class="fragment" -->![Data center picture](c03_assets/data_center.jpg)

---

### Searching the databases

* &shy;<!-- .element: class="fragment" -->Sequence databases can be queried in two ways:
    * &shy;<!-- .element: class="fragment" -->Obtain sequences from text input ([NCBI search](https://www.ncbi.nlm.nih.gov/))
    * &shy;<!-- .element: class="fragment" -->Obtain sequences from similar sequences ([BLAST search](https://blast.ncbi.nlm.nih.gov/Blast.cgi))

&shy;<!-- .element: class="fragment" -->![Database search icon](c03_assets/database-search.png)
    

---

### NCBI BLAST

* &shy;<!-- .element: class="fragment" -->Grab the FASTA file that resulted from the chromatogram analysis
* &shy;<!-- .element: class="fragment" -->Pick one of the sequences and query it against the [BLAST database](https://blast.ncbi.nlm.nih.gov/Blast.cgi)
    * &shy;<!-- .element: class="fragment" -->You want "Nucleotide BLAST"
    * &shy;<!-- .element: class="fragment" -->Use the "megablast" program
* &shy;<!-- .element: class="fragment" -->Find out What species the sequence you analysed belongs to!

<div class="r-stack">
  <img class="fragment" src="c03_assets/poke_back.webp">
  <img class="fragment" src="c03_assets/poke_back_2.webp">
</div>

---

### Text input

&shy;<!-- .element: class="fragment" -->![Picture of Psammodromus algirus](c03_assets/psammodromus.jpg)

* &shy;<!-- .element: class="fragment" -->Meet *Psammodromus algirus*
  * &shy;<!-- .element: class="fragment" -->Go to the [NCBI](https://www.ncbi.nlm.nih.gov/) website
  * &shy;<!-- .element: class="fragment" -->Select the "nucleotide" database
  * &shy;<!-- .element: class="fragment" -->Search for *Psammodromus algirus[organism], cytb[gene]*
* &shy;<!-- .element: class="fragment" -->Click "Send to" and get a file with all the sequences in FASTA format
  * &shy;<!-- .element: class="fragment" -->Save it as `~/sequences/P_algirus_NCBI.fasta`

---

### References

* [IUPAC reference](https://www.bioinformatics.org/sms/iupac.html)
* [How Sanger sequencing works](https://www.thermofisher.com/us/en/home/life-science/sequencing/sanger-sequencing/sanger-sequencing-workflow.html)
* [NCBI search terms list](https://www.ncbi.nlm.nih.gov/books/NBK49540/)
* [BLAST Documentation](https://www.ncbi.nlm.nih.gov/books/NBK153387/)
